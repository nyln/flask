# New

*Resource Packs
  * ✓ Add all maple leave variants bushy
  * ✓ Add holly / chestnut leaves bushy
  * Fix missing textures
    * Breaking bushes
    * Mob running particles
* Config
  * ✓ Backpacked
    * ✓ Remove config button from UI
    * ✓ Limit backpack to 3-6 Rows, dependant
  * ✓ Jade
  * ✓ BetterF3
* Blocks
  * Chest
    * Fix donkey not accepting modded chests
* Item
  * Add nametag recipe
* Food
  * More cookies
  * Missing cake/pie slices
  * Amethyst related food
* Tool
  * Backpack
    * Fix recipe to use tags
    * Make ingredients tagged
    * Remove old recipe
  * Amethyst equipment
* ✓ Coins
  * ✓ Better sprites
  * ✓ 3 Coins
  * ✓ Recipes
    * ✓ Making
      * ✓ 4 Copper Ingot + 1 Amethyst -> Penny
      * ✓ 4 Iron Ingot + 1 Emerald -> Shilling
      * ✓ 4 Gold Ingot + 1 Diamond -> Pound
    * ✓ Demaking
      * ✓ 1 Penny -> Blasting -> 1 Amethyst
      * ✓ 1 Shilling -> Blasting -> 1 Emerald
      * ✓ 1 Pound -> Blasting -> 1 Diamond