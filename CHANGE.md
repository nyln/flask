# To Add

## Waiting for new versions

https://www.curseforge.com/minecraft/mc-mods/bountiful (1.19.4) - either needs to downgrade to 1.19.2 or rest of mods need 1.19.4 updates
https://www.curseforge.com/minecraft/mc-mods/large-meals-an-add-on-for-farmers-delight (1.18.2)

# Removed
Removed in favor of json-things self made items
  { "projectID": 377056,  "fileID": 4277164,  "required": true, "url": "https://www.curseforge.com/minecraft/mc-mods/coins-je" },

Removed in favor of json-things (https://github.com/gigaherz/JsonThings/blob/master/documentation/Introduction.md)
  { "projectID": 354339,  "fileID": 3943067,  "required": true, "url": "https://www.curseforge.com/minecraft/mc-mods/open-loader" },


Placeholder
{ "projectID": ,  "fileID": 1000000,  "required": true, "url": "" }