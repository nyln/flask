Combined texture and resource pack, based on multiple different projects, merged and adjusted to encompass as much of the modpack as possible.

Credits to:
https://www.curseforge.com/minecraft/texture-packs/stay-true
https://www.curseforge.com/minecraft/texture-packs/abnormal-bushy-leaves
https://www.curseforge.com/minecraft/texture-packs/nekos-enhanced-tools-and-armor
https://www.curseforge.com/minecraft/texture-packs/better-vanilla-foods
https://www.curseforge.com/minecraft/texture-packs/fancy-boss-bars
https://www.curseforge.com/minecraft/texture-packs/visual-travelers-titles
